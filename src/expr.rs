use std::ops;

/* this was painful */
pub trait Operand:
    Sized
    + ops::Add<Self, Output = Self>
    + ops::Mul<Self, Output = Self>
    + ops::Sub<Self, Output = Self>
    + ops::Div<Self, Output = Self>
{
    fn sin(self) -> Self;
    fn cos(self) -> Self;
    fn tan(self) -> Self;
}
/*
impl<T: Sized +
        ops::Add<T, Output=T> +
        ops::Mul<T, Output=T> +
        ops::Sub<T, Output=T> +
        ops::Div<T, Output=T>> Operand for T {
            fn sin(&self) -> T;
}
*/
impl Operand for f32 {
    fn sin(self) -> f32 {
        self.sin()
    }
    fn cos(self) -> f32 {
        self.cos()
    }
    fn tan(self) -> f32 {
        self.tan()
    }
}

impl Operand for i32 {
    fn sin(self) -> i32 {
        (self as f32).sin() as i32
    }
    fn cos(self) -> i32 {
        (self as f32).cos() as i32
    }
    fn tan(self) -> i32 {
        (self as f32).tan() as i32
    }
}

#[derive(Debug, PartialEq)]
pub enum Operator {
    Add,
    Mul,
    Sub,
    Div,
}

#[derive(Debug, PartialEq)]
pub enum Expr<Operand> {
    Number(Operand),
    Operation(Operator, Box<Expr<Operand>>, Box<Expr<Operand>>),
    Function(String, Box<Expr<Operand>>),
}
