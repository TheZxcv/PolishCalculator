use std::io::BufReader;



use expression::evaluation::eval;
use expression::metaparser::infix2postfix;
use expression::parser::*;
use expression::tokenizer::*;

fn main() {
    let mut reader = BufReader::new(std::io::stdin());
    loop {
        let mut tokens = infix2postfix(tokenize::<f32>(&mut reader));

        if tokens.len() > 0 {
            let expr = parse(&mut tokens);
            print!("\t\t{}\n", eval(*expr));
        }
    }
}
