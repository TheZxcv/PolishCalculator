use crate::expr::Expr::*;
use crate::expr::Operator::*;
use crate::expr::*;

pub fn eval<T: Operand>(expr: Expr<T>) -> T {
    match expr {
        Number(n) => n,
        Operation(op, expr1, expr2) => (apply::<T>(op))(eval(*expr1), eval(*expr2)),
        Function(id, expr) => (apply_fn::<T>(id))(eval(*expr)),
    }
}

fn apply<T: Operand>(op: Operator) -> Box<dyn Fn(T, T) -> T> {
    match op {
        Add => Box::new(|a: T, b: T| a + b),
        Mul => Box::new(|a: T, b: T| a * b),
        Sub => Box::new(|a: T, b: T| a - b),
        Div => Box::new(|a: T, b: T| a / b),
    }
}

fn apply_fn<T: Operand>(id: String) -> Box<dyn Fn(T) -> T> {
    match id.as_ref() {
        "sin" => Box::new(|a: T| a.sin()),
        "cos" => Box::new(|a: T| a.cos()),
        "tan" => Box::new(|a: T| a.tan()),
        _ => panic!("no such function"),
    }
}
