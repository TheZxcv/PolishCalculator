use std::io::BufRead;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub enum Token<T> {
    NUM(T),
    FUN(String),
    LPAR,
    RPAR,
    PLUS,
    MINUS,
    STAR,
    SLASH,
}

use self::Token::*;
fn make_num<O: FromStr>(buffer: &str) -> Token<O> {
    assert!(!buffer.is_empty());
    match buffer.parse::<O>() {
        Ok(num) => NUM(num),
        Err(_) => panic!("error while parsing number"),
    }
}

fn make_fun<T>(buffer: &str) -> Token<T> {
    assert!(!buffer.is_empty());
    FUN(buffer.to_string())
}

use self::State::*;
enum State {
    NUMBER,
    IDENTIFIER,
    OTHER,
}

trait StateAction {
    fn make<T: FromStr>(&self, _: &str) -> Option<Token<T>>;
    fn is_appendable(&self, _: char) -> bool;
    fn next(_: char) -> State;
}

impl StateAction for State {
    fn make<T: FromStr>(&self, buffer: &str) -> Option<Token<T>> {
        match *self {
            NUMBER => Some(make_num(&buffer)),
            IDENTIFIER => Some(make_fun(&buffer)),
            OTHER => match buffer.chars().nth(0).unwrap() {
                '(' => Some(LPAR),
                ')' => Some(RPAR),
                '+' => Some(PLUS),
                '-' => Some(MINUS),
                '*' => Some(STAR),
                '/' => Some(SLASH),
                '\n' | ' ' => None,
                c => panic!("unrecognized token: {}", c),
            },
        }
    }

    fn is_appendable(&self, c: char) -> bool {
        match *self {
            NUMBER => c.is_digit(10) || c == '.',
            IDENTIFIER => c.is_alphabetic(),
            OTHER => false,
        }
    }

    fn next(c: char) -> State {
        if NUMBER.is_appendable(c) {
            NUMBER
        } else if IDENTIFIER.is_appendable(c) {
            IDENTIFIER
        } else {
            OTHER
        }
    }
}

pub fn tokenize<O: FromStr>(input: &mut dyn BufRead) -> Vec<Token<O>> {
    let mut line = String::new();
    match input.read_line(&mut line) {
        Err(why) => panic!("error while reading: {}", why),
        Ok(_) => (),
    }

    let mut state = OTHER;
    let mut buffer = String::new();
    let mut tokens: Vec<Token<O>> = Vec::new();
    for c in line.chars() {
        if !state.is_appendable(c) {
            if !buffer.is_empty() {
                state.make(&buffer).map(|t| tokens.push(t));
                buffer.clear();
            }
            state = State::next(c);
        }
        buffer.push(c);
    }

    if !buffer.is_empty() {
        state.make(&buffer).map(|t| tokens.push(t));
    }
    tokens
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::io::BufReader;

    fn test_tokenize(input: &str, expected: &[Token<i32>]) {
        let tokens = tokenize(&mut BufReader::new(input.as_bytes()));
        assert_eq!(tokens, expected);
    }

    #[test]
    fn test_tokenize_simple_expr() {
        test_tokenize("1 1 +", &[NUM(1), NUM(1), PLUS]);
    }

    #[test]
    fn test_tokenize_empty_expr() {
        test_tokenize("", &[]);
    }

    #[test]
    fn test_tokenize_fun() {
        test_tokenize("12 sin", &[NUM(12), FUN("sin".to_string())]);
    }

    #[test]
    fn test_tokenize_complex_expr() {
        test_tokenize(
            "4 2 5 * + 1 3 2 * + /",
            &[
                NUM(4),
                NUM(2),
                NUM(5),
                STAR,
                PLUS,
                NUM(1),
                NUM(3),
                NUM(2),
                STAR,
                PLUS,
                SLASH,
            ],
        );
    }

    #[test]
    fn test_tokenize_complex_expr_with_fun() {
        test_tokenize(
            "2 15 * sin 8 * 5 + 45 tan 2 + /",
            &[
                NUM(2),
                NUM(15),
                STAR,
                FUN("sin".to_string()),
                NUM(8),
                STAR,
                NUM(5),
                PLUS,
                NUM(45),
                FUN("tan".to_string()),
                NUM(2),
                PLUS,
                SLASH,
            ],
        );
    }

    #[test]
    fn test_tokenize_complex_expr_without_spaces() {
        test_tokenize(
            "2 15*sin8*5+45tan2+/",
            &[
                NUM(2),
                NUM(15),
                STAR,
                FUN("sin".to_string()),
                NUM(8),
                STAR,
                NUM(5),
                PLUS,
                NUM(45),
                FUN("tan".to_string()),
                NUM(2),
                PLUS,
                SLASH,
            ],
        );
    }
}
