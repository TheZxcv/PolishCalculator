use crate::tokenizer::Token;
use crate::tokenizer::Token::*;

use crate::expr::Expr::*;
use crate::expr::Operator::*;
use crate::expr::*;

pub fn parse<T>(tokens: &mut Vec<Token<T>>) -> Box<Expr<T>> {
    parse_polish(tokens)
}

fn parse_polish<T>(tokens: &mut Vec<Token<T>>) -> Box<Expr<T>> {
    let (ret, rest) = parse_polish_r(tokens);
    assert!(rest.is_empty());
    ret
}

fn parse_polish_r<T>(tokens: &mut Vec<Token<T>>) -> (Box<Expr<T>>, &Vec<Token<T>>) {
    let top = tokens.pop();
    if top.is_none() {
        panic!("Incomplete expression");
    }
    match top.unwrap() {
        RPAR | LPAR => panic!("unexpected token"),
        NUM(n) => (Box::new(Number(n)), tokens),
        FUN(id) => (Box::new(Function(id, parse_polish_r(tokens).0)), tokens),
        op => {
            let r = parse_polish_r(tokens).0;
            let l = parse_polish_r(tokens).0;

            match op {
                PLUS => (Box::new(Operation(Add, l, r)), tokens),
                MINUS => (Box::new(Operation(Sub, l, r)), tokens),
                STAR => (Box::new(Operation(Mul, l, r)), tokens),
                SLASH => (Box::new(Operation(Div, l, r)), tokens),
                _ => panic!("unexpected token"),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_number() {
        let mut tokens = vec![NUM(1)];
        let expr = Box::new(Number(1));
        assert_eq!(expr, parse::<i32>(&mut tokens))
    }

    #[test]
    fn test_simple_expr() {
        let mut tokens = vec![NUM(10), NUM(3), NUM(2), STAR, PLUS];
        let expr = Box::new(Operation(
            Add,
            Box::new(Number(10)),
            Box::new(Operation(Mul, Box::new(Number(3)), Box::new(Number(2)))),
        ));
        assert_eq!(expr, parse::<i32>(&mut tokens))
    }

    #[test]
    fn test_expr_with_fun() {
        let mut tokens = vec![NUM(10), FUN("sin".to_string())];
        let expr = Box::new(Function("sin".to_string(), Box::new(Number(10))));
        assert_eq!(expr, parse::<i32>(&mut tokens))
    }

    #[test]
    #[should_panic(expected = "Incomplete expression")]
    fn test_incomplete_expr() {
        let mut tokens = vec![NUM(10), PLUS];
        parse::<i32>(&mut tokens);
    }
}
