use crate::tokenizer::Token;
use crate::tokenizer::Token::*;

trait Operator {
    fn left_assoc(&self) -> bool;
    fn right_assoc(&self) -> bool;
    fn is_operator(&self) -> bool;
    fn precedence(&self) -> i32;
}

impl<T> Operator for Token<T> {
    fn left_assoc(&self) -> bool {
        match *self {
            PLUS | MINUS | STAR | SLASH => true,
            _ => false,
        }
    }
    fn right_assoc(&self) -> bool {
        false
    }
    fn precedence(&self) -> i32 {
        match *self {
            STAR | SLASH => 3,
            PLUS | MINUS => 2,
            _ => panic!("not an operator"),
        }
    }
    fn is_operator(&self) -> bool {
        match *self {
            PLUS | MINUS | STAR | SLASH => true,
            _ => false,
        }
    }
}

pub fn infix2postfix<T>(tokens: Vec<Token<T>>) -> Vec<Token<T>> {
    let mut output: Vec<Token<T>> = Vec::new();
    let mut stack: Vec<Token<T>> = Vec::new();

    for tok in tokens {
        match tok {
            NUM(_) => output.push(tok),
            FUN(_) => stack.push(tok),
            PLUS | MINUS | STAR | SLASH => {
                while !stack.is_empty()
                    && stack.last().unwrap().is_operator()
                    && tok.left_assoc()
                    && tok.precedence() <= stack.last().unwrap().precedence()
                {
                    output.push(stack.pop().unwrap());
                }
                stack.push(tok);
            }
            LPAR => stack.push(LPAR),
            RPAR => {
                let mut last = None;
                while let Some(op) = stack.pop() {
                    if op.is_operator() {
                        output.push(op);
                    } else {
                        last = Some(op);
                        break;
                    }
                }

                match last {
                    Some(LPAR) => (),
                    _ => panic!("missing parenthesis"),
                }

                if let Some(&FUN(_)) = stack.last() {
                    output.push(stack.pop().unwrap());
                }
            }
        }
    }
    while let Some(op) = stack.pop() {
        output.push(op);
    }
    output
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_simple() {
        let tokens = vec![NUM(1), PLUS, NUM(1)];
        let expected = vec![NUM(1), NUM(1), PLUS];
        assert_eq!(expected, infix2postfix::<i32>(tokens))
    }

    #[test]
    fn test_medium() {
        let tokens = vec![NUM(1), PLUS, NUM(1), STAR, NUM(2)];
        let expected = vec![NUM(1), NUM(1), NUM(2), STAR, PLUS];
        assert_eq!(expected, infix2postfix::<i32>(tokens))
    }

    #[test]
    fn test_simple_with_fun() {
        let tokens = vec![FUN("sin".to_string()), LPAR, NUM(1), PLUS, NUM(1), RPAR];
        let expected = vec![NUM(1), NUM(1), PLUS, FUN("sin".to_string())];
        assert_eq!(expected, infix2postfix::<i32>(tokens))
    }
}
