use std::io::BufReader;



use expression::evaluation::eval;
use expression::parser::parse;
use expression::tokenizer::tokenize;

#[test]
fn test_simple() {
    let input = "1 1 -";
    let output = eval::<i32>(*parse(&mut tokenize(&mut BufReader::new(input.as_bytes()))));
    assert_eq!(0, output);
}

#[test]
#[should_panic(expected = "Incomplete expression")]
fn test_incomplete() {
    let input = "2*";
    eval::<i32>(*parse(&mut tokenize(&mut BufReader::new(input.as_bytes()))));
}
